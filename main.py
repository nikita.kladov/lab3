import unittest


class TestUM(unittest.TestCase):

    def testAssertTrue(self):
       
        self.assertTrue(True)

    def testFailUnless(self):
        self.failUnless(True)

    def testFailIf(self):
        self.failIf(False)

    def testAssertFalse(self):

        self.assertFalse(False)

    def testEqual(self):

        self.failUnlessEqual(1, 3 - 2)

    def testNotEqual(self):
        
        self.failIfEqual(2, 3 - 2)

    def testEqualFail(self):
        
        self.failIfEqual(1, 2)

    def testNotEqualFail(self):
      
        self.failUnlessEqual(2, 3 - 1)

    def testNotAlmostEqual(self):
        
        self.failIfAlmostEqual(1.1, 3.3 - 2.0, places=1)

    def testAlmostEqual(self):
        
        self.failUnlessAlmostEqual(1.1, 3.3 - 2.0, places=0)


if __name__ == '__main__':
    unittest.main()
